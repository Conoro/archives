from django import forms

from Archives_app.models import Customers
from .models import FileParser, Customers


class FilterForm(forms.ModelForm):
    file = forms.FileField()

    class Meta:
        model = FileParser
        fields = ('customer', 'email_id', 'email_date', 'parse', 'options', 'sended', 'file', 'created_at')


class ExportForm(forms.Form):
    customer = forms.ModelChoiceField(queryset=Customers.objects.all())
    start_date = forms.DateTimeField()
    end_date = forms.DateTimeField()
    Filter_by = forms.ChoiceField(choices=[("email_date", "email_date"), ("customer_date", "customer_date")])


class CustomerForm(forms.ModelForm):
    class Meta:
        model = Customers
        fields = ('name', 'parser')
