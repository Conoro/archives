from django.db import models
import datetime


class Customers(models.Model):
    name = models.CharField(max_length=20)
    parser = models.CharField(max_length=20)

    def __str__(self):
        return self.name


class FileParser(models.Model):
    customer = models.ForeignKey(Customers, on_delete=models.CASCADE)
    email_id = models.IntegerField()
    email_date = models.DateTimeField()
    parse = models.CharField(max_length=20)
    options = models.CharField(max_length=20)
    sended = models.BooleanField()
    file = models.FileField(null=True, blank=None)
    created_at = models.DateTimeField()

    def __str__(self):
        return self.customer.name
