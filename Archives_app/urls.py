from django.urls import path
from .views import *

urlpatterns = [
    path('', HomeTemplate.as_view(), name='home'),
    path('export', FilerTemplate.as_view(), name='export'),
    path('customer', CustomerTemplate.as_view(), name='customer')
]
