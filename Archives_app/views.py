from django.views.generic import ListView, DetailView, View, FormView, CreateView
from django.shortcuts import render, redirect

from Archives_app.models import Customers
from .models import FileParser
import pandas
import datetime
from .forms import FilterForm, ExportForm, CustomerForm


class HomeTemplate(CreateView):
    model = FileParser
    form_class = FilterForm
    template_name = 'index.html'
    success_url = ''


class FilerTemplate(FormView):
    form_class = ExportForm
    template_name = 'ExportTemplate.html'

    def post(self, request, *args, **kwargs):
        filter = str(self.request.POST['Filter_by'])
        if filter == "customer_date":

            if FileParser.objects.filter(created_at__gte=self.request.POST['start_date'],
                                         created_at__lte=self.request.POST['end_date'],
                                         customer=self.request.POST['customer']):
                type = []
                crate_date = []
                customer = []
                list = []
                expo = []
                sent = []
                info = FileParser.objects.filter(created_at__gte=self.request.POST['start_date'],
                                                 created_at__lte=self.request.POST['end_date'],
                                                 customer=self.request.POST['customer'])
                for inf in info:
                    type.append(str(inf.file))
                    crate_date.append(str(inf.created_at))
                    customer.append(inf.customer.name)
                    sent.append((inf.sended))
                for i in range(0, len(info)):
                    list.append([type[i], crate_date[i], customer[i], sent[i]])
                df = pandas.DataFrame({'type': type, 'Date ': crate_date, 'customer': customer, 'sended': sent})
                link = f'media/informe{datetime.datetime.now()}.xlsx'
                writer = pandas.ExcelWriter(link, engine='xlsxwriter')
                df.to_excel(writer, sheet_name='Sheet1')
                writer.save()
                context = {'list': list, 'link': link}
                return render(request, "table.html", context)
            else:
                return redirect('export')
        else:
            if FileParser.objects.filter(email_date__gte=self.request.POST['start_date'],
                                         email_date__lte=self.request.POST['end_date'],
                                         customer=self.request.POST['customer']):
                type = []
                crate_date = []
                customer = []
                sent = []
                list = []
                expo = []
                info = FileParser.objects.filter(email_date__gte=self.request.POST['start_date'],
                                                 email_date__lte=self.request.POST['end_date'],
                                                 customer=self.request.POST['customer'])
                for inf in info:
                    type.append(str(inf.file))
                    crate_date.append(str(inf.email_date))
                    customer.append(inf.customer.name)
                    sent.append((inf.sended))
                for i in range(0, len(info)):
                    list.append([type[i], crate_date[i], customer[i], sent[i]])
                df = pandas.DataFrame({'type': type, 'Date ': crate_date, 'customer': customer, 'sended': sent})
                link = f'media/informe{datetime.datetime.now()}.xlsx'
                writer = pandas.ExcelWriter(link, engine='xlsxwriter')
                df.to_excel(writer, sheet_name='Sheet1')
                writer.save()
                context = {'list': list, 'link': link}
                return render(request, "table.html", context)
            else:
                return redirect('export')


class CustomerTemplate(CreateView):
    model = Customers
    form_class = CustomerForm
    template_name = 'CustomerTemplate.html'
    success_url = ''
